import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule, Routes} from '@angular/router';
import {UserOverviewComponent} from './modules/user-overview/user-overview.component';
import {HttpClientModule} from '@angular/common/http';
import {MatButtonModule, MatCardModule} from '@angular/material';
import { UserCardComponent } from './modules/user-card/user-card.component';
import { DetailUserComponent } from './modules/detail-user/detail-user.component';
import { PurchaseCardComponent } from './modules/detail-user/purchase-card/purchase-card.component';

const appRoutes: Routes = [
  {path: '', component: UserOverviewComponent},
  {path: 'detailUser/:id', component: DetailUserComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    UserOverviewComponent,
    UserCardComponent,
    DetailUserComponent,
    PurchaseCardComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    RouterModule.forRoot(appRoutes, {enableTracing: true}),
    HttpClientModule,
    MatCardModule,
    MatButtonModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
