import {Purchase} from './purchase';

export interface User {
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
  street: string;
  streetNr: string;
  postCode: string;
  cityName: string;
  purchases: Purchase[];
}
