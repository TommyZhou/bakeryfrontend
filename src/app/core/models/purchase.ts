export interface Purchase {
  purchaseName: string;
  price: number;
  date: string;
}
