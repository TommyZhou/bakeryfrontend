import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../../models/user';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(environment.baseUrl + '/api/users');
  }

  getUserById(userId: string): Observable<User>{
    return this.http.get<User>(environment.baseUrl + '/api/users/' + userId);
  }
}
