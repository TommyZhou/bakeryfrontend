import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../core/models/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent implements OnInit {
  @Input() user: User;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateToDetailUser() {
    this.router.navigate(['/detailUser', this.user._id]);
  }

}
