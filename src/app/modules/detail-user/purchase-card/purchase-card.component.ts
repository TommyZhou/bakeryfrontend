import {Component, Input, OnInit} from '@angular/core';
import {Purchase} from '../../../core/models/purchase';

@Component({
  selector: 'app-purchase-card',
  templateUrl: './purchase-card.component.html',
  styleUrls: ['./purchase-card.component.css']
})
export class PurchaseCardComponent implements OnInit {
  @Input() purchase: Purchase;

  constructor() {
  }

  ngOnInit() {
  }

}
