import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {User} from '../../core/models/user';
import {UserService} from '../../core/services/user/user.service';
import {Purchase} from '../../core/models/purchase';

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.css']
})
export class DetailUserComponent implements OnInit {
  userId: string;
  user: User;
  moneySpentThisYear = 0;

  constructor(private userService: UserService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.userId = this.route.snapshot.paramMap.get('id');
    this.userService.getUserById(this.userId).subscribe(user => {
      this.user = user;
      this.getMoneySpent(user.purchases);
    });
  }

  getMoneySpent(purchases: Purchase[]) {
    for (const purchase of purchases) {
      const date = new Date(purchase.date);
      if (new Date().getFullYear() === date.getFullYear()) {
        this.moneySpentThisYear += purchase.price;
      }
    }
  }
}
